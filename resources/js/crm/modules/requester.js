import axios from "axios";

let axiosInstance = axios.create({
    baseURL:"https://crm.manucadeaux.com/api/",
    headers: { 'content-type': 'application/json' },
    responseType:'json',
});

let api = () => {
    let token = localStorage.getItem("token");
    if(token) axiosInstance.defaults.headers.common["Authorization"] = "Bearer "+token;
    return axiosInstance;
};

export default {



    signIn(formData){
        return api().post("sign/in",formData)
    },
    signUp(formData){
        return api().post("sign/up",formData)
    },
    signData(){
        return api().get("sign/data")
    },
    signout(){
        return api().get("sign/out");
    },

    //--------- test data ---------//
    activitiesGet(){
        return api().get("test/activities/get")
    },
    transitionsGet(){
        return api().get("test/transitions/get")
    },
    serviceTurnover(val){
        return api().get("test/service/turnover/"+val[0]+"/"+val[1])
    },
    comapnySiren(siren){
        return api().get("test/company/siren/"+siren)
    },

    regionalGrant(region,budget,naf){
        return api().get("test/grants/region/"+region+"/"+budget+"/"+naf)
    },
    cpnGrant(service,budget){
        return api().get("/test/grants/cpn/"+service+"/"+budget)
    },
    saveContact(formData){
        return api().post("/test/contact/save", formData)
    },
    getEvents(){
        return api().get("test/events/get")
    },
    addEvents(formData){
        return api().post("test/events/add",formData)
    },
    generateZoom(cid){
        return api().post("test/zoom/generate",cid)
    },
    confirmContact(contactData){
        return api().post("test/contact/confirm",contactData)
    },
    saveTimer(timer){
        return api().post("test/timer/save",timer);
    },

    //--------- leads data ---------//
    getContactsLeads(id){
        return api().get("leads/contacts/all/"+id)
    },

    rensendConfirmationMail(data){
        return api().post("leads/email/resend",data)
    },

    //--------- supervisor data ---------//
    getClients(){
        return api().get("supervisor/clients/get")
    },

    //--------- calendar data ---------//

    calendarEventsGet(){
        return api().get("calendar/events/get")
    },

    //--------- profile data ---------//
    saveImage(img){
        return api().post("profile/image/save", img);
    },

    updateprofile(formData){
        return api().post("profile/update/save", formData)
    },
    confirmclient(formData){
        return api().post("mail/confirm", formData)
    },
    getrepertoir(id){
        return api().get("reperatoir/get/"+id)
    },
    getstat(id){
        return api().get("statistique/get/"+id)
    },
    getUsers(){
        return api().get("users/get")
    },
    delelead(id){
        return api().get("users/delete/"+id)
    },
    validUsers(id){
        return api().get("users/Valid/"+id)
    },
    saveContactsCommercial(formData){
        return api().post("reperatoir/save", formData);
    },
}
