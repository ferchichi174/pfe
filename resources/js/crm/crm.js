import Vue from "vue";
import 'vuetify/dist/vuetify.min.css';
import Vuetify from "vuetify";
import 'element-ui/lib/theme-chalk/index.css';
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/fr'


import Crm from "./Crm.vue";
import router from "./modules/router";
import store from "./modules/store";

import VueDatetimePickerJs from 'vue-date-time-picker-js';
Vue.use(VueDatetimePickerJs, {
    name: 'custom-date-picker',
    props: {
        inputFormat: 'YYYY-MM-DD HH:mm',
        format: 'YYYY-MM-DD HH:mm',
        editable: false,
        inputClass: 'form-control my-custom-class-name',
        placeholder: 'sélectionner une date',
        altFormat: 'YYYY-MM-DD HH:mm',
        color: '#00acc1',
        autoSubmit: false,
        //...
        //... And whatever you want to set as default
        //...
    }
});
Vue.config.productionTip = false;

Vue.use(Vuetify);
Vue.use(ElementUI, { locale })
Vue.component('crm',Crm)
const EventBus = new Vue();
Vue.prototype.$bus = EventBus;

const app = new Vue({
  el: '#main',
  vuetify: new Vuetify(),
  router,
  store,
  render: h => h(Crm),
});
