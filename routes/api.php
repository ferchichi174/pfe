<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CRM\SignController;
use App\Http\Controllers\CRM\TestController;
use App\Http\Controllers\CRM\LeadsController;
use App\Http\Controllers\CRM\ProfileController;
use App\Http\Controllers\CRM\CalendarController;
use App\Http\Controllers\CRM\ClientsController;
use App\Http\Controllers\CRM\ArticlesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get("clients/get", [ClientsController::class, "getClients"]);
Route::post("mail/confirm", [ClientsController::class, "confirmMeet"]);

Route::post("avis/save", [ClientsController::class, "clientAvis"]);
Route::get("reperatoir/get/{id}", [ClientsController::class, "getrepreratoir"]);
Route::get("statistique/get/{id}", [ClientsController::class, "getstatistic"]);
Route::post("reperatoir/save", [ClientsController::class, "addrepartoir"]);
Route::middleware(["auth:api"])->group(function(){
Route::get("users/get",[ProfileController::class, "getUsers"]);});
Route::get("articles/get",[ArticlesController::class, "getArticles"]);
Route::post("articles/add",[ArticlesController::class, "addArticle"]);
Route::get("articles/delete/{id}",[ArticlesController::class, "DeleteArticle"]);
Route::post("articles/edit",[ArticlesController::class, "EditArticle"]);



Route::get("users/delete/{id}",[ProfileController::class, "DeleteUser"]);
Route::get("users/Valid/{id}",[ProfileController::class, "ValidUser"]);
Route::post("repertoir/simple-excel/import", [ClientsController::class, 'import'])->name('excel.import');

Route::post("forgot-password",[SignController::class,"forgot_password"]);
Route::prefix("sign")->group(function(){
  Route::post("in",[SignController::class,"signIn"]);
  Route::post("up",[SignController::class,"signUp"]);
  Route::middleware(['auth:api'])->get('data', [SignController::class,"signData"]);
  Route::middleware(['auth:api'])->get('out',  [SignController::class,"signOut"]);
});

Route::middleware(["auth:api"])->group(function(){

  Route::prefix("test")->group(function(){

    Route::post("calendcpn/add",[TestController::class, "calendarcpn"]);
    Route::get("activities/get",[TestController::class,"activitiesGet"]);
    Route::get("transitions/get", [TestController::class,"transitionsGet"]);
    Route::get("events/get", [TestController::class,"eventsGet"]);
    Route::post("events/add",[TestController::class,"eventsAdd"]);

    Route::get("service/turnover/{min}/{max}", [TestController::class,"serviceTurnover"]);
    Route::get("company/siren/{siren}",[TestController::class,"comapnySiren"]);

    Route::post("contact/save",[TestController::class,"saveContact"]);
    Route::post("contact/confirm",[TestController::class,"confirmContact"]);

    Route::post("zoom/generate",[TestController::class,"generateZoom"]);

    Route::prefix("grants")->group(function(){
        Route::get("cpn/{service}/{budget}",[TestController::class,"cpnGrant"]);
        Route::get("region/{region}/{budget}/{naf}",[TestController::class,"regionGrant"]);
    });

    Route::post("timer/save", [TestController::class, "saveTimer"]);

  });

  Route::prefix("leads")->group(function(){
    // obtenir la liste des contacts selon l'utilisateur
    Route::get("contacts/all/{id}",[LeadsController::class,"getLeadsList"]);
    Route::get("contacts/{cid}",[LeadsController::class,"getLeadContact"]);
    //
    Route::post("email/resend",[LeadsController::class,"resendMail"]);
    Route::post("contacts/etat",[LeadsController::class,"changetat"]);

  });

  Route::prefix("calendar")->group(function(){
    Route::get("events/get",[CalendarController::class,"getEvents"]);
  });

  Route::prefix("profile")->group(function(){
    Route::post("image/save",[ProfileController::class, "saveImage"]);
    Route::post("update/save",[ProfileController::class, "updateprofile"]);
  });

  Route::prefix("supervisor")->group(function(){
    Route::get("calendar/events", [CalendarController::class,"getEvents"]);
    Route::get("clients/get", [ClientsController::class, "getClients"]);
    Route::get("clients/simple-excel/export", [ClientsController::class, 'export'])->name('excel.export');
  });

    Route::get("ring/get",[TestController::class,"ring"]);

});




