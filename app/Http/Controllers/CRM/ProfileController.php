<?php

namespace App\Http\Controllers\CRM;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function saveImage(Request $request)
    {
        return response()->json($request->all());
    }
    public function getUsers(){
        if (Auth::id() == 2) {

        $user = User::orderby("id","desc")->get();
        return response()->json(["users"=>$user]);}
    }
public function DeleteUser($id){
    try {
    User::where("id",$id)->delete();

        return response()->json(["error"=>false,"message"=>"Utilisateur supprimé avec succès"]);
    }
    catch (QueryException $qe) {
        return response()->json(["error"=>true,"message"=>$qe]);
    }

    }

    public function ValidUser($id){

        try {
            $user =  User::where("id",$id)->first();
            $user->authorized = 1;
            $user->update();
            return response()->json(["error"=>false,"message"=>"Utilisateur Validé avec succès"]);
        }
        catch (QueryException $qe) {
            return response()->json(["error"=>true,"message"=>$qe]);
        }

    }

    public function updateprofile(Request $request){
        try {
            $user =  User::where("id",$request->id)->first();
            $user->tel = $request->tel;
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->adresse = $request->adresse;
            $user->email = $request->email;
            $user->update();
            return response()->json(["error"=>false,"message"=>"profile mis a jour avec succès"]);
        }
        catch (QueryException $qe) {
            return response()->json(["error"=>true,"message"=>$qe]);
        }

    }


}
